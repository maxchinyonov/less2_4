#!/bin/bash
token_value='glpat-pd-Acr3vgPgV3dv-XhEY'
project_id='31438014' # fork2
#project_id='31996309' # less 2.1
#project_id='31835618' # dind
read -p "Enter the job name : " job_name
read -p "Enter the job ending status (success/failed)  : " job_status

last_job_id=$(curl -N --silent --header "PRIVATE-TOKEN: $token_value" "https://gitlab.com/api/v4/projects/$project_id/jobs" | jq '.[] | .id, .name, .status' |  grep -C1 $job_name | grep -B2 $job_status | head -1)


if
        [ $last_job_id -ne 0 > /dev/null 2>&1 ]
   then             
          echo -e "\033[32mFind $job_status for job $job_name id $last_job_id\e[0m"
          read -p "Do you want to print all output yes/no ?  : " answer
           if [[ $answer == yes ]];
             then
               curl -N --silent --header "PRIVATE-TOKEN: $token_value" "https://gitlab.com/api/v4/projects/$project_id/jobs/$last_job_id" | jq .
            fi
	    if [[ $answer =~ ^(yes|no)$  ]];
             then
                echo -e "\033[32m      Finish     \e[0m"
             else
                echo -e "\033[41m$answer invalid pattern\e[0m"
            fi
   else
        echo -e "\033[41mNo $job_status for job $job_name or job doesnt exist in history of gitlab-api\e[0m"
fi
