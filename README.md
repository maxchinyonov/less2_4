<h3 class="code-line" data-line-start=0 data-line-end=1 ><a id="__0"></a>Описание задания</h3>
<p class="has-line-data" data-line-start="1" data-line-end="3">Часто нужно копировать соседний проект для сборки или деплоя приложения ,давайте это сделаем.<br>
Ничего заумного но это нужно знать.</p>
<ol>
<li class="has-line-data" data-line-start="3" data-line-end="4">Создать проект с одним файлом Ридми (приватный)</li>
<li class="has-line-data" data-line-start="4" data-line-end="7">Скопировать в пайплайне (в единственной джобе) в наш проект из прошлого задания (тот который с композом для старта проекта)<br>
<a href="https://docs.gitlab.com/ee/ci/ssh_keys/#ssh-keys-when-using-the-docker-executor">https://docs.gitlab.com/ee/ci/ssh_keys/#ssh-keys-when-using-the-docker-executor</a></li>
</ol>
<h3 class="code-line" data-line-start=7 data-line-end=8 ><a id="_7"></a>Решение</h3>
<p class="has-line-data" data-line-start="8" data-line-end="11">Пайп - <a href="https://gitlab.com/maxchinyonov/less2_4/-/jobs/1910016885">https://gitlab.com/maxchinyonov/less2_4/-/jobs/1910016885</a><br>
Репа - <a href="https://gitlab.com/maxchinyonov/less2_4">https://gitlab.com/maxchinyonov/less2_4</a><br>
Тут стандартный вариант, как в доке.</p>
<ol>
<li class="has-line-data" data-line-start="11" data-line-end="12">Генерим локально пару ключей ssh.</li>
<li class="has-line-data" data-line-start="12" data-line-end="13">Добавляем в проект новую переменную SSH_PRIVATE_KEY, чтобы потом этот приватный ключ засунуть в докер раннер в рамках пайплайна.</li>
<li class="has-line-data" data-line-start="13" data-line-end="14">Публичный ключ добавляем в проект less2_7 как deploy key.</li>
<li class="has-line-data" data-line-start="14" data-line-end="15">Прописываем всё это дело в .gitlab-ci, чтобы контейнер раннера скачал ssh клиент и подсовываем туда наш приватный ключ.</li>
<li class="has-line-data" data-line-start="15" data-line-end="16">Делаем гит клон.</li>
<li class="has-line-data" data-line-start="16" data-line-end="17">Готово, вы восхитительны.</li>
</ol>
