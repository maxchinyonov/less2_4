#!/bin/bash
LESS2_1_ID=31996309
last_pipeline_less_2_1=$(curl -k --location --header "PRIVATE-TOKEN: ${MY_TOKEN}" "https://gitlab.com/api/v4/projects/${LESS2_1_ID}/pipelines/" | jq -r '.[] | "\(.id)"' | head -1)
job_build_nginx_prod=$(curl -k --location --header "PRIVATE-TOKEN: ${MY_TOKEN}" "https://gitlab.com/api/v4/projects/${LESS2_1_ID}/pipelines/$last_pipeline_less_2_1/jobs" | jq -r '.[] | "\(.id) \(.name)"' | grep -i build_nginx_prod | cut -f1 -d ' ')
job_build_django_prod=$(curl -k --location --header "PRIVATE-TOKEN: ${MY_TOKEN}" "https://gitlab.com/api/v4/projects/${LESS2_1_ID}/pipelines/$last_pipeline_less_2_1/jobs" | jq -r '.[] | "\(.id) \(.name)"' | grep -i build_prod | cut -f1 -d ' ')
job_build_nginx_dev=$(curl -k --location --header "PRIVATE-TOKEN: ${MY_TOKEN}" "https://gitlab.com/api/v4/projects/${LESS2_1_ID}/pipelines/$last_pipeline_less_2_1/jobs" | jq -r '.[] | "\(.id) \(.name)"' | grep -i build_nginx_dev | cut -f1 -d ' ')
job_build_django_dev=$(curl -k --location --header "PRIVATE-TOKEN: ${MY_TOKEN}" "https://gitlab.com/api/v4/projects/${LESS2_1_ID}/pipelines/$last_pipeline_less_2_1/jobs" | jq -r '.[] | "\(.id) \(.name)"' | grep -i build_dev | cut -f1 -d ' ')


if [[ $REMOTE_PROJECT_REF == master ]];

             then

curl --location --output "${PROJECT_LOCAL_DIR}/artifact_build_nginx_prod_${CI_JOB_ID}" --header "PRIVATE-TOKEN: ${MY_TOKEN}" "https://gitlab.com/api/v4/projects/${LESS2_1_ID}/jobs/${job_build_nginx_prod}/artifacts/${tag_nginx_prod}artifact.txt";
curl --location --output "${PROJECT_LOCAL_DIR}/artifact_build_django_prod_${CI_JOB_ID}" --header "PRIVATE-TOKEN: ${MY_TOKEN}" "https://gitlab.com/api/v4/projects/${LESS2_1_ID}/jobs/${job_build_django_prod}/artifacts/${tag_prod}artifact.txt"

fi
                      
if [[ $REMOTE_PROJECT_REF == dev ]];     

              then 

curl --location --output "${PROJECT_LOCAL_DIR}/artifact_build_nginx_dev_${CI_JOB_ID}" --header "PRIVATE-TOKEN: ${MY_TOKEN}" "https://gitlab.com/api/v4/projects/${LESS2_1_ID}/jobs/${job_build_nginx_dev}/artifacts/${tag_nginx_dev}artifact.txt";
curl --location --output "${PROJECT_LOCAL_DIR}/artifact_build_django_dev_${CI_JOB_ID}" --header "PRIVATE-TOKEN: ${MY_TOKEN}" "https://gitlab.com/api/v4/projects/${LESS2_1_ID}/jobs/${job_build_django_dev}/artifacts/${tag_dev}artifact.txt"              

fi
