#!/bin/sh
last_commit_sha=$(curl -k --silent --header "PRIVATE-TOKEN: ${MY_TOKEN}" https://gitlab.com/api/v4/projects/32271519/repository/commits | cut -f1 -d "," | cut -f4 -d "\"")
curl -k --silent --header "PRIVATE-TOKEN: ${MY_TOKEN}" https://gitlab.com/api/v4/projects/32271519/repository/archive?sha=${last_commit_sha} -o From_Less2_7.tar.gz
tar -xf From_Less2_7.tar.gz # --strip-components=1
yes | rm From_Less2_7.tar.gz
